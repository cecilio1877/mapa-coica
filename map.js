let map = L.map('map').setView([-1,-60.66], 5)

// crear mapa base
L.tileLayer('https://mt1.google.com/vt/lyrs=y&x={x}&y={y}&z={z}',{
    attribution: '© <a href="https://osm.org/copyright">OpenStreetMap<\/a> contributors'
}).addTo(map)

let icono_base = L.Icon.extend({
    options:{
        iconSize:[40, 40],
        //shadowSize:[80, 84],
        //shadowAnchor: [4, 62]

    }
})

let icono1 = new icono_base({iconUrl: 'https://coicamazonia.org/wp-content/uploads/2022/02/aidesep.png'})
let icono2 = new icono_base({iconUrl: 'https://coicamazonia.org/wp-content/uploads/2022/02/apa.png'})
let icono3 = new icono_base({iconUrl: 'https://coicamazonia.org/wp-content/uploads/2022/02/cidob.png'})
let icono4 = new icono_base({iconUrl: 'https://coicamazonia.org/wp-content/uploads/2022/02/coiab.png'})
let icono5 = new icono_base({iconUrl: 'https://coicamazonia.org/wp-content/uploads/2022/02/confeniae.png'})
let icono6 = new icono_base({iconUrl: 'https://coicamazonia.org/wp-content/uploads/2022/02/foag.png'})
let icono7 = new icono_base({iconUrl: 'https://coicamazonia.org/wp-content/uploads/2022/02/ois.png'})
let icono8 = new icono_base({iconUrl: 'https://coicamazonia.org/wp-content/uploads/2022/02/opiac.png'})
let icono9 = new icono_base({iconUrl: 'https://coicamazonia.org/wp-content/uploads/2022/02/orpia.png'})

L.marker([-3.469557,-74.487305],{icon:icono1}).on('click', onClick).addTo(map)
L.marker([3.557283,-58.798828],{icon:icono2}).on('click', onClick).addTo(map)
L.marker([-12.425848,-66.621094],{icon:icono3}).on('click', onClick).addTo(map)
L.marker([-1.669686,-64.511719],{icon:icono4}).on('click', onClick).addTo(map)
L.marker([-0.642309, -77.065319],{icon:icono5}).on('click', onClick).addTo(map)
L.marker([4.390229,-53.129883],{icon:icono6}).on('click', onClick).addTo(map)
L.marker([4.803829, -56.026571],{icon:icono7}).on('click', onClick).addTo(map)
L.marker([1.01069,-71.71875],{icon:icono8}).on('click', onClick).addTo(map)
L.marker([4.302591,-66.005859],{icon:icono9}).on('click', onClick).addTo(map)


function onClick() {
    let win = window.open('https://coicamazonia.org/somos/#org_miembros')
    win.focus();
}

map.scrollWheelZoom.disable();

